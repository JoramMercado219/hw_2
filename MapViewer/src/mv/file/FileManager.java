/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonArray;

import javafx.scene.shape.Polygon;
import java.util.ArrayList;
import javafx.scene.layout.Pane;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import mv.data.DataManager;
import mv.data.Region;
import mv.gui.Workspace;
import mv.MapViewerApp;
import saf.ui.AppGUI;
import saf.AppTemplate;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
    AppTemplate app;
    
    static final String JSON_NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String JSON_SUBREGIONS = "SUBREGIONS";
    static final String JSON_NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    static final String JSON_X = "X";
    static final String JSON_Y = "Y";
    
    //String filePathExport;
    
    //public void setFilePath(String filePath){
    //    filePathExport = filePath;
    //}

    //@Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        
        
        
        //clear data out
        DataManager dataManager = (DataManager)data;
        dataManager.reset();
        
        //dataManager.getRegions().removeAll();
        
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //AppGUI gui = (AppGUI)app.getGUI();
        
        //LOAD JSON FILE
        JsonObject json = loadJSONFile(filePath);
        
        //GET NUMBER OF SUBREGIONS
        int numberOfSubRegions = json.getInt(JSON_NUMBER_OF_SUBREGIONS);
        
        //LOAD ARRAY OF REGIONS
        JsonArray jsonSubRegionsArray = json.getJsonArray(JSON_SUBREGIONS);
        //LOOP THROUGH ARRAY OF JSON REGIONS
        for (int i =0;  i < numberOfSubRegions; i++){
            
            //GET JSON 1 REGION
	    JsonObject jsonRegion = jsonSubRegionsArray.getJsonObject(i);
            
            //CREATE 1 NEW REGION
            Region newRegion = new Region();
            
            //GET NUMBER OF JSON POLYGONS IN JSON 1 REGION
            int numberOfPolygonsInRegion = jsonRegion.getInt(JSON_NUMBER_OF_SUBREGION_POLYGONS);
            
            JsonArray jsonPolygonsArray = jsonRegion.getJsonArray(JSON_SUBREGION_POLYGONS);
            
            //SET NUMBER OF POLYGONS IN 1 NEW REGION
            newRegion.setNumberOfPolygons(numberOfPolygonsInRegion);
            
            //LOOP THROUGH ARRAY OF JSON POLYGONS IN 1 REGION
            //for (int k = 0;  k < jsonRegion.getInt(JSON_NUMBER_OF_SUBREGION_POLYGONS); k++){
              for (int k = 0;  k < numberOfPolygonsInRegion; k++){
                
                //GET 1 JSON POLYGON IN 1 JSON REGION 
                //IS COMPOSED OF AN ARRAY OF 2-COORDINATE POINTS
                JsonArray jsonPolygon = jsonPolygonsArray.getJsonArray(k);
                
                //GET NUMBER OF POINTS FROM 1 JSON POLYGON
                int numberOfPolygonPoints = jsonPolygon.size();
                
                //TO STORE ALL COORDINATE VALUES, X and Y, FROM 1 JSON POLYGON
                ArrayList points = new ArrayList<Double>();
          
                // Wrong ?? -- JsonArray polygonPoints = jsonPolygon.getJsonArray("");
                
                //GET POINT ARRAY (2 VALUES) REPRESENTING X and Y coordinates FOR 1 POINT 
                for (int index2 = 0; index2 < numberOfPolygonPoints; index2++){
                JsonObject jsonPolygonPoint = jsonPolygon.getJsonObject(index2);
                
                // GET THE SIZE OF THE SCREEN
                Screen screen = Screen.getPrimary();
                
               
                
                Rectangle2D bounds = screen.getVisualBounds();               
                
                //BorderPane testAppPane = new BorderPane();
                //Scene testScene = new Scene(testAppPane);
                //Stage testStage = new Stage();
                //testStage.setX(bounds.getMinX());
                //testStage.setY(bounds.getMinY());
                //testStage.setWidth(bounds.getWidth());
                //testStage.setHeight(bounds.getHeight());
                //testStage.setScene(testScene);
               
                //double xMax = testAppPane.getCenter().getLayoutBounds().getMaxX();
                //double yMax = testAppPane.getCenter().getLayoutBounds().getMaxY();
                
                double xMax = bounds.getMaxX()-16;
                double yMax = bounds.getMaxY()-99;
                
                //double xMax = gui.getAppPane().centerProperty().getValue().boundsInLocalProperty().getValue().getMaxX();
                //double yMax = gui.getAppPane().centerProperty().getValue().boundsInLocalProperty().getValue().getMaxY();               
                //double xMax = workspace.getInnerPaneWidth();
                //double yMax = workspace.getInnerPaneHieght();
                double x = 0;
                double y = 0;
                
                //LOOP THROUGHT JSON NUMBER OF POINTS FROM 1 POLYGON

                //GET JSON POINT
                //JsonObject onePoint = jsonPolygonPointsArray.getJsonObject(j);  
                //GET COORDINATE VALUES FOR 1 POINT
                double coordX = getDataAsDouble(jsonPolygonPoint, JSON_X);
                double coordY = getDataAsDouble(jsonPolygonPoint, JSON_Y);
                        
                x = ((coordX+180.0)/360.0)*xMax;
                y = yMax-(((coordY+90.0)/180.0)*yMax);
                        
                points.add(x);
                points.add(y);                       
                        
                }
                
                //TO COPY FROM ARRAYLIST to ARRAY ALL COORDINATE VALUES OF 1 POLYGON
                double pointsArray[] = new double[numberOfPolygonPoints*2];
                
                //COPY COORDINATE VALUES OF 1 POLYGON TO ARRAY FROM ARRAY LIST
                for (int index = 0; index < numberOfPolygonPoints*2; index++){
                    pointsArray[index] = (double)points.get(index);
                }
                
                //CREATE POLYGON WITH GIVEN COORDINATES
                Polygon P1 = new Polygon(pointsArray);
                
                //SET FILL COLOR AND OUTLINE COLOR
                P1.setFill(Color.GREEN);
                P1.setStroke(Color.BLACK);
                newRegion.addOnePolygon(P1);
            }
            dataManager.addRegion(newRegion);
        }
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //@Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
