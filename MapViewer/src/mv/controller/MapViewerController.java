/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.controller;

import javafx.geometry.Rectangle2D;
import javafx.scene.layout.Pane;
import mv.gui.Workspace;
import mv.MapViewerApp;
import mv.data.DataManager;
import mv.data.Region;
import javafx.scene.shape.Polygon;
import javafx.stage.Screen;
import saf.ui.AppGUI;
import javafx.scene.shape.Line;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.paint.Color;
/**
 *
 * @author Arim
 */
public class MapViewerController {
     MapViewerApp app;
     
     protected int count = 0;
     
     protected ObservableList<Line> linesO;
    
     public MapViewerController(MapViewerApp initApp){
             app = initApp;
     }
     
     public void zoomIn(double x, double y){

                Workspace workspace = (Workspace)app.getWorkspaceComponent();
                
                Pane moveInnerPane = workspace.getInnerPane();
                
                double standardX = moveInnerPane.getScene().widthProperty().get()+2;
                double standardY = moveInnerPane.getScene().heightProperty().get()-60;
                
                double centerX = standardX/2;
                double centerY = standardY/2;
                  
                moveInnerPane.setScaleX(moveInnerPane.getScaleX()*2.0);
                moveInnerPane.setScaleY(moveInnerPane.getScaleY()*2.0);                
                
                moveInnerPane.setLayoutX(moveInnerPane.getScaleX()*(standardX-centerX-x));
                moveInnerPane.setLayoutY(moveInnerPane.getScaleY()*(standardY-centerY-y));
                   
     }
     
     public void zoomOut(double x, double y){

                Workspace workspace = (Workspace)app.getWorkspaceComponent();

                Pane moveInnerPane = workspace.getInnerPane();              
                double standardX = moveInnerPane.getScene().widthProperty().get()+2;
                double standardY = moveInnerPane.getScene().heightProperty().get()-60;
                
                double centerX = standardX/2;
                double centerY = standardY/2;                
                
                moveInnerPane.setScaleX(moveInnerPane.getScaleX()*0.5);
                moveInnerPane.setScaleY(moveInnerPane.getScaleY()*0.5);
                
                moveInnerPane.setLayoutX(moveInnerPane.getScaleX()*(standardX-centerX-x));
                moveInnerPane.setLayoutY(moveInnerPane.getScaleY()*(standardY-centerY-y));
           
     }  
     
     public void goLeft(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();   
        Pane moveInnerPane = workspace.getInnerPane();
                        
        moveInnerPane.setLayoutX(moveInnerPane.getLayoutX()+10);
     }
     
     public void goRight(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent(); 
        Pane moveInnerPane = workspace.getInnerPane();
        
        moveInnerPane.setLayoutX(moveInnerPane.getLayoutX()-10);
     }
     
     public void goUp(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();   
        Pane moveInnerPane = workspace.getInnerPane();
                        
        moveInnerPane.setLayoutY(moveInnerPane.getLayoutY()+10);
     }
     
     public void goDown(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent(); 
        Pane moveInnerPane = workspace.getInnerPane();
        
        moveInnerPane.setLayoutY(moveInnerPane.getLayoutY()-10);
     }
     
     public void gridLines(){
         Workspace workspace = (Workspace)app.getWorkspaceComponent(); 
         AppGUI gui = (AppGUI)app.getGUI();
         
         Pane modify = workspace.getInnerPane();
         
         double standardX = (modify.widthProperty().get())/12.0;
         double standardY = (modify.heightProperty().get())/6.0;
         
         double MaxX = modify.getScene().getWidth();
         double MaxY = modify.getScene().getHeight();         
         
         if (count == 0){
         
         ObservableList<Line> lines = FXCollections.observableArrayList();
        
         Line line1 = new Line(0.0, 0.0, 0.0, MaxY);
         line1.setStroke(Color.WHITE);
         Line line2 = new Line(standardX, 0.0, standardX, MaxY);
         line2.setStroke(Color.GREY);
         line2.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line3 = new Line(2*standardX, 0.0, 2*standardX, MaxY);
         line3.setStroke(Color.GREY);
         line3.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line4 = new Line(3*standardX, 0.0, 3*standardX, MaxY);
         line4.setStroke(Color.GREY);
         line4.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         Line line5 = new Line(4*standardX, 0.0, 4*standardX, MaxY);
         line5.setStroke(Color.GREY);
         line5.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line6 = new Line(5*standardX, 0.0, 5*standardX, MaxY);
         line6.setStroke(Color.GREY);
         line6.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line7 = new Line(6*standardX, 0.0, 6*standardX, MaxY);
         line7.setStroke(Color.WHITE);

         Line line8 = new Line(7*standardX, 0.0, 7*standardX, MaxY);
         line8.setStroke(Color.GREY);
         line8.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line9 = new Line(8*standardX, 0.0, 8*standardX, MaxY);
         line9.setStroke(Color.GREY);
         line9.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line10 = new Line(9*standardX, 0.0, 9*standardX, MaxY);
         line10.setStroke(Color.GREY);
         line10.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line11 = new Line(10*standardX, 0.0, 10*standardX, MaxY);
         line11.setStroke(Color.GREY);
         line11.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);
         Line line12 = new Line(11*standardX, 0.0, 11*standardX, MaxY);
         line12.setStroke(Color.GREY);
         line12.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         Line line13 = new Line(12*standardX, 0.0, 12*standardX, MaxY);
         line13.setStroke(Color.WHITE);

         Line line14 = new Line(0.0, 0.0, MaxX, 0.0);
         line14.setStroke(Color.GREY);
         line14.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         Line line15 = new Line(0.0, standardY, MaxX, standardY);
         line15.setStroke(Color.GREY);
         line15.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         Line line16 = new Line(0.0, 2*standardY, MaxX, 2*standardY); 
         line16.setStroke(Color.GREY);
         line16.getStrokeDashArray().addAll(22d, 18d, 7d, 30d);  
         Line line17 = new Line(0.0, 3*standardY, MaxX, 3*standardY);
         line17.setStroke(Color.WHITE);
         Line line18 = new Line(0.0, 4*standardY, MaxX, 4*standardY);
         line18.setStroke(Color.GREY);
         line18.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         Line line19 = new Line(0.0, 5*standardY, MaxX, 5*standardY);
         line19.setStroke(Color.GREY);
         line19.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         Line line20 = new Line(0.0, 6*standardY, MaxX, 6*standardY);
         line20.setStroke(Color.GREY);
         line20.getStrokeDashArray().addAll(22d, 18d, 7d, 30d); 
         
         lines.addAll(line1,line2,line3,line4,line5,line6,line7, line8,
         line9,line10,line11,line12,line13,line14,line15,line16,line17,line18,line19,line20);
         
         this.linesO = lines;
         modify.getChildren().addAll(linesO);
         }
         
         
         if(count%2 == 0 && count>0)
            modify.getChildren().addAll(linesO);
         else if( count%2 != 0 && count>0)
             modify.getChildren().removeAll(linesO);
         
         
         count++;
         
     }
     
}
