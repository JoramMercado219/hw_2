/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import saf.ui.AppGUI;

//import mv.controller.ToDoListController;
import mv.data.Region;
import mv.data.DataManager;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import mv.PropertyType;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import mv.controller.MapViewerController;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    //static final String CLASS_BORDERED_PANE = "bordered_pane";    
    //this is the app
    MapViewerApp app;
    AppGUI gui;
    
    Label headingLabel;
    
    //BorderPane OuterPane;
 
    Pane innerPane;
    
    
    // THIS CONTROLLER PROVIDES THE RESPONSES TO INTERACTIONS
    MapViewerController mapViewerController;
    
    
    public Pane getInnerPane(){
        return innerPane;
    }
    
    public Pane getWorkspacePane(){
        return workspace;
    }
    
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        gui = app.getGUI();
        
        layoutGUI();
        
        
        FlowPane fileToolbarPane;
        
        Scene primaryScene;
        
        primaryScene = gui.getPrimaryScene();
        primaryScene.fillProperty().set(Color.BLUE);
        
        //Pane pane;
        //pane = (Pane)gui.getAppPane().getCenter();
        //pane.getChildren().get(0).
        
        fileToolbarPane = (FlowPane)gui.getAppPane().getTop();
        fileToolbarPane.getChildren().remove(0);
        fileToolbarPane.getChildren().remove(1);
        
 
        workspace = new Pane();
        //workspace.sceneProperty().get().fillProperty().set(Color.BLUE);
        //gui.getAppPane().getCenter().getScene().fillProperty().set(Color.AQUA); 
        
       
    }
    
    private void setupHandlers() {
        //innerPane.focusTraversableProperty().set(true);
	// MAKE THE CONTROLLER
	mapViewerController = new MapViewerController(app);
        AppGUI gui = (AppGUI)app.getGUI();      
        gui.getAppPane().getTop().requestFocus();
        
        
        innerPane.setFocusTraversable(true);
        
        gui.getPrimaryScene().setOnKeyPressed(e->{
            if(e.getCode() == KeyCode.LEFT)
                mapViewerController.goLeft();
            if(e.getCode() == KeyCode.RIGHT)
                mapViewerController.goRight();
            if(e.getCode() == KeyCode.UP)
                mapViewerController.goUp();
            if(e.getCode() == KeyCode.DOWN)
                mapViewerController.goDown();
             if(e.getCode() == KeyCode.G)
                mapViewerController.gridLines();             
        });
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        innerPane.setOnMousePressed(e->{            
            double xCoord = e.getX();
            double yCoord = e.getY();            
            if (e.isPrimaryButtonDown()){
                mapViewerController.zoomIn(xCoord, yCoord);
            }
            else{
                mapViewerController.zoomOut(xCoord, yCoord);
            }  
        });
        
    }
    
    private void layoutGUI(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        headingLabel = new Label();
        headingLabel.setText(props.getProperty(PropertyType.WORKSPACE_HEADING_LABEL));
        
        DataManager dataManager = (DataManager)app.getDataComponent();
        ObservableList<Region> regions = dataManager.getRegions();
        
        if(regions != null){
            int numberOfRegions = regions.size();
        
            for (int i = 0; i < numberOfRegions; i++){
                innerPane.getChildren().addAll(regions.get(i).getPolygons());
            }     
            workspace.getChildren().addAll(innerPane);
        }
        
    }

    @Override
    public void reloadWorkspace() {
        AppGUI gui = (AppGUI)app.getGUI();
        
        innerPane.getChildren().removeAll();
        innerPane.getChildren().clear();
        workspace.getChildren().removeAll(innerPane);
        workspace.getChildren().clear();
        
        DataManager dataManager = (DataManager)app.getDataComponent();
        ObservableList<Region> regions = dataManager.getRegions();

        if(regions != null){
            int numberOfRegions = regions.size();
        
            for (int i = 0; i < numberOfRegions; i++){
                innerPane.getChildren().addAll(regions.get(i).getPolygons());
            }     
            workspace.getChildren().addAll(innerPane);
        }        

        setupHandlers();         
    }
    
    //public double getInnerPaneHieght(){
    //    return innerPane.boundsInLocalProperty().getValue().getMaxY();
    //}
    
    //public double getInnerPaneWidth(){
    //    return innerPane.boundsInLocalProperty().getValue().getMaxX();
    //}    

    @Override
    public void initStyle() {
 
        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        innerPane = new Pane();
        
        
    }
}
