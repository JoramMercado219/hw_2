/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.scene.shape.Polygon;
import javafx.collections.FXCollections;

/**
 *
 * @author Arim
 */
public class Region {
    
    public static final int DEFAULT_NUMBER_OF_SUBREGION_POLYGONS = 1;
    //public static final int DEFAULT_SUBREGION_POLYGONS = 1;
    //public static final String DEFAULT_X = "X";
    //public static final String DEFAULT_Y = "Y";
    
    IntegerProperty NUMBER_OF_SUBREGION_POLYGONS;
    ObservableList<Polygon> polygons;
    
    public Region() {
        NUMBER_OF_SUBREGION_POLYGONS = 
                new SimpleIntegerProperty(DEFAULT_NUMBER_OF_SUBREGION_POLYGONS);
        polygons = FXCollections.observableArrayList();
    }
    
    
    public Region(int numberOfPolygons){
        NUMBER_OF_SUBREGION_POLYGONS = 
                new SimpleIntegerProperty(numberOfPolygons);
        polygons = FXCollections.observableArrayList();        
    }
    
    public IntegerProperty getNumberOfPolygonsProperty(){
        return NUMBER_OF_SUBREGION_POLYGONS;
    }
    
    public int getNumberOfPolygons(){
        return NUMBER_OF_SUBREGION_POLYGONS.get();
    }
    
    public void setNumberOfPolygons(int newNumberOfPolygons){
        NUMBER_OF_SUBREGION_POLYGONS.set(newNumberOfPolygons);
    }
    
    public ObservableList<Polygon> getPolygons(){
        return polygons;
    }
    
    public void setPolygons(ObservableList<Polygon> newPolygons){
        polygons = newPolygons;
    }
    
    public void addOnePolygon(Polygon P1){
        polygons.add(P1);
    }
}
