package mv.data;

import javafx.scene.shape.Polygon;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import mv.gui.Workspace;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    
    ObservableList<Region> regions;
    
    
    MapViewerApp app;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
    }
    
    public void addRegion(Region newRegion){
        regions.add(newRegion);
    }
    
    public void removeRegions(){
        ObservableList<Region> getAllRegions = getRegions();
        int numberOfRegions = getAllRegions.size();
        for (int x = 0; x < numberOfRegions; x++){
            getAllRegions.remove(x);
        }
    }
    
    public ObservableList<Region> getRegions(){
        return regions;
    }
    
    @Override
    public void reset() {
        //regions.removeAll(regions);
        regions = FXCollections.observableArrayList();
        removeRegions();
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        //workspace.getWorkspace().
        
    }
}
